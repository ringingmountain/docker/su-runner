#!/usr/bin/env sh
set -e

. ci/common.sh

if test -n "${CI_COMMIT_TAG}"
then
  DOCKER_TAG="${CI_COMMIT_TAG}"
else
  DOCKER_TAG="$(echo "${CI_COMMIT_SHA}" | cut -c1-7)"
fi

echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

for DISTRO in alpine debian
do
  LATEST="${CI_REGISTRY_IMAGE}:latest-${DISTRO}"
  TAG="${CI_REGISTRY_IMAGE}:${DOCKER_TAG}-${DISTRO}"

  # Pull image for layer cache, ignore pull failures
  ! docker pull "${LATEST}"

  log_run 'Building Docker image' docker build --pull --cache-from "${LATEST}" --tag "${LATEST}" --tag "${TAG}" "${DISTRO}"

  for VERSION in "${LATEST}" "${TAG}"
  do
    log_run "Pushing Docker image ${VERSION}" docker push "${VERSION}"
  done
done
