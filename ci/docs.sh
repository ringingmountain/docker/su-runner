#!/usr/bin/env sh
set -e

. ci/common.sh

log_run_quiet 'Installing dependencies' pip install -q Sphinx

log_run_quiet 'Building documentation' sphinx-build docs build/docs
