FROM debian:stretch

LABEL url="https://gitlab.com/ringingmountain/docker/su-runner/debian" authors='robin@ringingmountain.com'

ENV GOSU_VERSION=1.11

WORKDIR /sbin

COPY run /sbin

RUN apt-get update && \
    apt-get -y --no-install-recommends install ca-certificates curl && \
    apt-get -y install gpg && \
    # Fetch gosu public key
    gpg --keyserver ipv4.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && \
    # Fetch, verify, and install gosu
    curl -o gosu -SL "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$(dpkg --print-architecture)" && \
    curl -o gosu.asc -SL "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$(dpkg --print-architecture).asc" && \
    gpg --verify gosu.asc && \
    rm gosu.asc && \
    # Set permissions on gosu and gosu-runner
    chmod a+x gosu run && \
    # Cleanup
    apt-get -y remove ca-certificates curl gpg && \
    apt-get -y autoremove && \
    apt-get clean

ENTRYPOINT ["/sbin/run"]
