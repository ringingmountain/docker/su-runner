Su Runner
=========

A Docker image that runs a command as a provided user and optional group.

It comes in both Debian and Alpine flavors.

+---------------+---------------------------------------------------------------+
| Repository    | https://gitlab.com/ringingmountain/docker/su-runner           |
+---------------+---------------------------------------------------------------+
| CI Pipeline   | https://gitlab.com/ringingmountain/docker/su-runner/pipelines |
+---------------+---------------------------------------------------------------+
| Documentation | https://su-runner.readthedocs.io/                             |
+---------------+---------------------------------------------------------------+

|pipeline| |docs|


Usage
-----


As a Command
~~~~~~~~~~~~

.. code-block:: bash

   docker run -e USER_ID="$(id -u $USER)" -e GROUP_ID="$(id -g $USER)" ringingmountain/su-runner:latest-debian /command/to/run


As a Base Image
~~~~~~~~~~~~~~~

.. code-block:: docker

   FROM ringingmountain/su-runner:latest-debian

   ENTRYPOINT ["/sbin/run", "/command/to/run"]


.. code-block:: docker

   FROM ringingmountain/su-runner:latest-alpine

   ENTRYPOINT ["/sbin/run", "/command/to/run"]


As a Builder
~~~~~~~~~~~~

.. code-block:: docker

   FROM my-debian-based-image

   COPY --from=ringingmountain/su-runner:latest-debian /sbin/gosu /sbin
   COPY --from=ringingmountain/su-runner:latest-debian /sbin/run /sbin

   ENTRYPOINT ["/sbin/run", "/command/to/run"]


.. code-block:: docker

   FROM my-alpine-based-image

   COPY --from=ringingmountain/su-runner:latest-alpine /sbin/su-exec /sbin
   COPY --from=ringingmountain/su-runner:latest-alpine /sbin/run /sbin

   ENTRYPOINT ["/sbin/run", "/command/to/run"]


Notes
~~~~~

The ``USER_ID`` environment variable must be set in the Docker environment to the numeric ID of the user to run the command as.
A system user named ``su-runner-user`` is created with the provided user ID.

If the ``GROUP_ID`` environment variable is set:
# A group named ``su-runner-group`` is created with the provided group ID.
# The ``su-runner-user`` is added to it.
# The command is executed as ``GROUP_ID:USER_ID``

Your container's architecture must match that of the ``su-runner`` image you select.

You can also use ``CMD`` for your script instead of adding to the ``ENTRYPOINT``, but be aware of the differences that entails.


Quickstart Development Guide
----------------------------


Build Documentation
~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   ci/docs.sh


Build Docker Image
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

  docker build -t ringingmountain/su-runner:local-debian ./debian
  docker build -t ringingmountain/su-runner:local-alpine ./alpine


Run Gitlab CI Jobs Locally
~~~~~~~~~~~~~~~~~~~~~~~~~~

You will first need to install the `Gitlab runner`_ package and `register`_ a runner on your local machine.

.. code-block:: bash

   gitlab-runner exec docker <job_name> --docker-services docker:dind --docker-privileged


Releasing a New Version
~~~~~~~~~~~~~~~~~~~~~~~

1. Checkout master.
2. Decide whether you are releasing a major, minor, or patch revision.
   For assistance in making this choice see the `SemVer`_ standard.
3. Ensure an entry for the version exists in ``docs/history.rst`` summarizing the changes you are releasing.
4. Update the version in docs/conf.py
5. Commit the changes, commenting that you are bumping the version.
6. Tag the repo with the matching version.
7. Push to the central remote and your fork.


 .. |pipeline| image:: https://gitlab.com/ringingmountain/docker/su-runner/badges/master/pipeline.svg
                :target: https://gitlab.com/ringingmountain/docker/su-runner/pipelines
..  |docs|     image:: https://readthedocs.org/projects/su-runner/badge/?version=latest
                :target: https://su-runner.readthedocs.io/

.. _Gitlab runner: https://docs.gitlab.com/runner/install/
.. _register: https://docs.gitlab.com/runner/register/index.html#one-line-registration-command
.. _SemVer: https://semver.org
